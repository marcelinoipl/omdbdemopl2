//
//  OMDBClientTest.swift
//  OMDBDemoPL2
//
//  Created by Luis Marcelino on 24/11/15.
//  Copyright © 2015 Empresa Imaginada. All rights reserved.
//

import XCTest

class OMDBClientTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSearchMovies () {
        
        let expetation = expectationWithDescription("getting love")
        
        OMDBClient.searchMovies("love") { (movies) -> Void in
            XCTAssertNotNil(movies)
            
            expetation.fulfill()
        }
        
        waitForExpectationsWithTimeout(5) { (error) -> Void in
            XCTAssertNil(error)
        }
    }
    
    func testParseMovies() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let resultString = "{\"Search\":[{\"Title\":\"Tomorrow Never Dies\",\"Year\":\"1997\",\"imdbID\":\"tt0120347\",\"Type\":\"movie\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTM1MTk2ODQxNV5BMl5BanBnXkFtZTcwOTY5MDg0NA@@._V1_SX300.jpg\"},{\"Title\":\"Never Let Me Go\",\"Year\":\"2010\",\"imdbID\":\"tt1334260\",\"Type\":\"movie\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTM3NDQ1MjE2OF5BMl5BanBnXkFtZTcwNDIxNTk2Mw@@._V1_SX300.jpg\"},{\"Title\":\"Wall Street: Money Never Sleeps\",\"Year\":\"2010\",\"imdbID\":\"tt1027718\",\"Type\":\"movie\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTU5MDEzMzYwMF5BMl5BanBnXkFtZTcwNTcwMjUxMw@@._V1_SX300.jpg\"},{\"Title\":\"Never Back Down\",\"Year\":\"2008\",\"imdbID\":\"tt1023111\",\"Type\":\"movie\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTkzNDg3MTIyMF5BMl5BanBnXkFtZTcwOTAwNDc1MQ@@._V1_SX300.jpg\"},{\"Title\":\"Justin Bieber: Never Say Never\",\"Year\":\"2011\",\"imdbID\":\"tt1702443\",\"Type\":\"movie\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTY0NDQzMjIzOF5BMl5BanBnXkFtZTcwNDk2NzczNA@@._V1_SX300.jpg\"},{\"Title\":\"Never Been Kissed\",\"Year\":\"1999\",\"imdbID\":\"tt0151738\",\"Type\":\"movie\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTQyODI1Njg3Ml5BMl5BanBnXkFtZTcwNTI0MDcyMQ@@._V1_SX300.jpg\"},{\"Title\":\"Never Say Never Again\",\"Year\":\"1983\",\"imdbID\":\"tt0086006\",\"Type\":\"movie\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTM1NjgzMDkwOF5BMl5BanBnXkFtZTcwMzM4NzI0NA@@._V1_SX300.jpg\"},{\"Title\":\"I Could Never Be Your Woman\",\"Year\":\"2007\",\"imdbID\":\"tt0466839\",\"Type\":\"movie\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMTY4NDc2MDA3MV5BMl5BanBnXkFtZTcwNTU5MzI0MQ@@._V1_SX300.jpg\"},{\"Title\":\"Metallica Through the Never\",\"Year\":\"2013\",\"imdbID\":\"tt2172935\",\"Type\":\"movie\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BMjQwNjk5MTk4Ml5BMl5BanBnXkFtZTcwNTgwNDA5OQ@@._V1_SX300.jpg\"},{\"Title\":\"Return to Never Land\",\"Year\":\"2002\",\"imdbID\":\"tt0280030\",\"Type\":\"movie\",\"Poster\":\"http://ia.media-imdb.com/images/M/MV5BODI5MTk1MDM3N15BMl5BanBnXkFtZTcwNzM1MjI1MQ@@._V1_SX300.jpg\"}]}"
        let responseData = resultString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let movies = OMDBClient.parseMovies(responseData)
        XCTAssertNotNil(movies)
        XCTAssertEqual(movies?.count, 10)
        XCTAssertEqual(movies![0].title, "Tomorrow Never Dies")
        
        XCTAssertNil(OMDBClient.parseMovies(nil))
        
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
