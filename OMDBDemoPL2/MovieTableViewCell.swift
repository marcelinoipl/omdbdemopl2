//
//  MovieTableViewCell.swift
//  OMDBDemoPL2
//
//  Created by Luis Marcelino on 20/11/15.
//  Copyright © 2015 Empresa Imaginada. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var yearLabel: UILabel!
    
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var posterImage: UIImageView!
    
    func setMovie(movie:Movie) {
        self.titleLabel.text = movie.title
        self.yearLabel.text = movie.year
        self.typeLabel.text = movie.type
        if let urlString = movie.poster {
            if let url = NSURL(string: urlString) {
                let session = NSURLSession.sharedSession()
                let task = session.dataTaskWithURL(url, completionHandler: { (data, response, error) -> Void in
                    if let imageData = data {
                        let image = UIImage(data: imageData)
                        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                            // Must run in main thread
                            self.posterImage.image = image
                        })
                    }
                })
                task.resume()
            }
        }
        
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
