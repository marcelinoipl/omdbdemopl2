//
//  OMDBClient.swift
//  OMDBDemoPL2
//
//  Created by Luis Marcelino on 20/11/15.
//  Copyright © 2015 Empresa Imaginada. All rights reserved.
//

import Foundation


class OMDBClient {
    static func searchMovies(searchText:String, completionHandler: ([Movie]?) -> Void) {
        guard let url = NSURL(string: "http://www.omdbapi.com/?s=" + searchText) else {
            return
        }
        // url exists!!!
        
        let session = NSURLSession.sharedSession()
        let dataTask = session.dataTaskWithURL(url) { (data, response, error) -> Void in
            let movies = OMDBClient.parseMovies(data)
            completionHandler(movies)
        }
        dataTask.resume()
        
    }
    
    static func parseMovies(data:NSData?) -> [Movie]? {
        guard let movieData = data else {
            // data is nil
            return nil
        }
        // movieData exists!!
        do {
            if let resultDic = try NSJSONSerialization.JSONObjectWithData(movieData, options: NSJSONReadingOptions.AllowFragments) as? NSDictionary {
                if let jsonArray = resultDic.objectForKey("Search") as? NSArray {
                    // local var to return
                    var movies = [Movie]()
                    for (var i = 0 ; i < jsonArray.count ; i++) {
                        if let jsonMovie = jsonArray.objectAtIndex(i) as? NSDictionary {
                            let ttle = jsonMovie.objectForKey("Title") as! String
                            let yr = jsonMovie.objectForKey("Year") as! String
                            let typ = jsonMovie.objectForKey("Type") as! String
                            let poster = jsonMovie.objectForKey("Poster") as? String
                            let imdbId = jsonMovie.objectForKey("imdbID") as! String
                            let movie = Movie(title: ttle, year: yr, type: typ, poster: poster, imdbId: imdbId)
                            movies.append(movie)
                        }
                    }
                    
                    return movies
                }
            }
        }
        catch {
            print(error)
        }
        return nil
    }
    
    
}