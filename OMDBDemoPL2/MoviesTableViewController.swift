//
//  MoviesTableViewController.swift
//  OMDBDemoPL2
//
//  Created by Luis Marcelino on 24/11/15.
//  Copyright © 2015 Empresa Imaginada. All rights reserved.
//

import UIKit
import FBSDKShareKit

class MoviesTableViewController: UITableViewController, UISearchBarDelegate {

    var movies:[Movie]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        let title = NSLocalizedString("Welcome title", comment: "Error message")
        let msg = NSLocalizedString("Welcome message", comment: "")
        let alertView = UIAlertView(title: title, message: msg, delegate: nil, cancelButtonTitle: "OK")
        alertView.show()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return movies?.count ?? 0
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("movieCell", forIndexPath: indexPath) as! MovieTableViewCell

        // get the movie from the array of movies
        let movie = movies![indexPath.row]
        cell.setMovie(movie)

        return cell
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let movie = self.movies![indexPath.row]
//        if let url = NSURL(string: "http://www.imdb.com/title/" + movie.imdbId) {
//            UIApplication.sharedApplication().openURL(url)
//        }

        let movieCell = tableView.cellForRowAtIndexPath(indexPath) as! MovieTableViewCell
        let photo = FBSDKSharePhoto()
        photo.image = movieCell.posterImage.image

        let content = FBSDKSharePhotoContent()
        content.photos = [photo]
        
        FBSDKShareDialog.showFromViewController(self, withContent: content, delegate: nil).show()

    }
    /*
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // Mark: - UISearchBarDelegate
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        guard var searchText = searchBar.text else {
            return
        }
        searchText = searchText.stringByReplacingOccurrencesOfString(" ", withString: "+")
        OMDBClient.searchMovies(searchText) { (movies) -> Void in
            self.movies = movies
            NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                // main thread!!!
                self.tableView.reloadData()
            })

        }
        
        
    }

}
