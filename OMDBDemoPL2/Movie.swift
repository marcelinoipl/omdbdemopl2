//
//  Movie.swift
//  OMDBDemoPL2
//
//  Created by Luis Marcelino on 20/11/15.
//  Copyright © 2015 Empresa Imaginada. All rights reserved.
//

import Foundation

struct Movie {
    let title:String
    let year:String
    let type:String
    let poster:String?
    let imdbId:String
}